# frozen_string_literal: true

module CommissionCalculator
  autoload :BaseService, 'commission_calculator/base_service'
  autoload :Calculator, 'commission_calculator/calculator'
  autoload :CommissionAmount, 'commission_calculator/commission_amount'
  autoload :CommissionPercentage, 'commission_calculator/commission_percentage'
  autoload :Amount, 'commission_calculator/amount'
  autoload :Version, 'commission_calculator/version'
end
