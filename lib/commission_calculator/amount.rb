# frozen_string_literal: true

module CommissionCalculator
  class Amount < BaseService
    def initialize(amount)
      @value = Float(amount)
    end

    private

    attr_reader :value
  end
end
