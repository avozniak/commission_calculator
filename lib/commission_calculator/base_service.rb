# frozen_string_literal: true

module CommissionCalculator
  class BaseService
    extend Forwardable

    def_delegators :value, :negative?, :zero?

    def self.call(*args)
      new(*args).call
    end

    def call
      raise ArgumentError, 'Value should be greater than zero!' if negative_or_zero?

      value
    end

    private

    def negative_or_zero?
      negative? || zero?
    end
  end
end
