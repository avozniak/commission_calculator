# frozen_string_literal: true

RSpec.describe CommissionCalculator::Calculator do
  let(:options) { {} }

  subject { described_class.call(options) }

  shared_examples 'raises exeption' do
    ['asdf', '0', -123].each do |value|
      context "when value is #{value}" do
        let(:value) { value }

        specify { expect { subject }.to raise_error ArgumentError }
      end
    end
  end

  describe '.call' do
    context 'with amount' do
      context 'with valid value' do
        before { options.merge! amount: 100 }

        it { is_expected.to eq [79.0, 21.0] }
      end

      context 'with invalid value' do
        it_behaves_like 'raises exeption'
      end
    end

    context 'without amount' do
      specify { expect { subject }.to raise_error ArgumentError }
    end

    context 'with commission_amount' do
      before { options.merge! amount: 100, commission_amount: value }

      context 'with valid value' do
        let(:value) { 5 }

        it { is_expected.to eq [75.0, 25.0] }
      end

      context 'with invalid value' do
        it_behaves_like 'raises exeption'
      end
    end

    context 'with commission percentage' do
      before { options.merge! amount: 100, commission_amount: value }

      context 'with valid value' do
        let(:value) { 50 }

        it { is_expected.to eq [30.0, 70.0] }
      end

      context 'with invalid value' do
        it_behaves_like 'raises exeption'
      end
    end

    context 'with commission entity' do
      before { options.merge! amount: 100, commission_entity: commission_entity }

      context 'when commission amount is defined in entity' do
        let(:commission_entity) { double('DummyClass', commission_amount: 3) }

        it { is_expected.to eq [77.0, 23.0] }
      end

      context 'when commission amount is not defined in entity' do
        let(:commission_entity) { double('DummyClass', commission_amount: nil) }

        it { is_expected.to eq [79.0, 21.0] }
      end
    end
  end
end
