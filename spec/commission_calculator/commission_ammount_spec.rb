# frozen_string_literal: true

RSpec.describe CommissionCalculator::CommissionAmount do
  subject { described_class.call(options) }

  describe '.call' do
    context 'with specified commission_amount' do
      context 'with valid value' do
        let(:options) { { commission_amount: 5 } }

        it { is_expected.to eq 5 }
      end

      context 'with invalid value' do
        [0, -123, 'asfd'].each do |value|
          let(:options) { { commission_amount: value } }

          specify { expect { subject }.to raise_error ArgumentError }
        end
      end
    end

    context 'without specified commission_amount' do
      let(:options) { {} }

      it { is_expected.to eq 1 }
    end

    context 'with specified commission_entity' do
      let(:options) { { commission_entity: entity } }

      context 'with defined commission_amount' do
        let(:entity) { double('DummyClass', commission_amount: 5) }

        it { is_expected.to eq 5 }
      end

      context 'without defined commission_amount' do
        let(:entity) { double('DummyClass', commission_amount: nil) }

        it { is_expected.to eq 1 }
      end
    end
  end
end
