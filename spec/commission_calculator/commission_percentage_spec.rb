# frozen_string_literal: true

RSpec.describe CommissionCalculator::CommissionPercentage do
  subject { described_class.call(options) }

  describe '.call' do
    context 'with specified commission_percentage' do
      context 'with valid value' do
        let(:options) { { commission_percentage: 5 } }

        it { is_expected.to eq 0.05 }
      end

      context 'with invalid value' do
        [0, -123, 'asfd'].each do |value|
          let(:options) { { commission_percentage: value } }

          specify { expect { subject }.to raise_error ArgumentError }
        end
      end
    end

    context 'without specified commission_amount' do
      let(:options) { {} }

      it { is_expected.to eq 0.2 }
    end
  end
end
