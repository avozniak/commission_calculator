# frozen_string_literal: true

module CommissionCalculator
  class Calculator < BaseService
    def initialize(amount:, **options)
      @amount = Amount.call(amount)
      @options = options
    end

    def call
      [amount_without_commission, commission]
    end

    private

    attr_reader :amount, :options

    def commission_amount
      @commission_amount = CommissionAmount.call(options)
    end

    def commission_percentage
      @commission_percentage = CommissionPercentage.call(options)
    end

    def commission
      amount * commission_percentage + commission_amount
    end

    def amount_without_commission
      amount - commission
    end
  end
end
