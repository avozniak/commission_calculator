# frozen_string_literal: true

module CommissionCalculator
  class CommissionAmount < BaseService
    DEFAULT_COMMISION_AMOUNT = 1.0

    def initialize(options)
      @commission_amount = Float(options.fetch(:commission_amount, DEFAULT_COMMISION_AMOUNT))
      @commission_entity = options[:commission_entity]
    end

    private

    def value
      commission_entity&.commission_amount || commission_amount
    end

    attr_reader :commission_amount, :commission_entity
  end
end
