# frozen_string_literal: true

RSpec.describe CommissionCalculator::Amount do
  describe '.call' do
    subject { described_class.call(value) }

    shared_examples 'invalid value' do
      specify { expect { subject }.to raise_error ArgumentError }
    end

    context 'when value is number' do
      context 'positize' do
        let(:value) { 123 }

        it { is_expected.to eq 123 }
      end

      context 'negative' do
        let(:value) { -123 }

        it_behaves_like 'invalid value'
      end

      context 'zero' do
        let(:value) { 0 }

        it_behaves_like 'invalid value'
      end
    end

    context 'when amount in not a number' do
      let(:value) { 'forty-two' }

      it_behaves_like 'invalid value'
    end
  end
end
