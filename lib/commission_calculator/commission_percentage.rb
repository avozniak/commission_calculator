# frozen_string_literal: true

module CommissionCalculator
  class CommissionPercentage < BaseService
    DEFAULT_PERCENTAGE = 20

    def initialize(options)
      @percentage = Float(options.fetch(:commission_percentage, DEFAULT_PERCENTAGE))
    end

    private

    def value
      percentage / 100
    end

    attr_reader :percentage
  end
end
